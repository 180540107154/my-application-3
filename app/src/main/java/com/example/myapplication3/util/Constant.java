package com.example.myapplication3.util;

public class Constant {

    public static final String FIRST_NAME = "FirstName";
    public static final String MOBILE_NUMBER = "Mobile_Number";
    public static final String EMAIL_ADDRESS = "Email_Address";
    public static final String GENDER = "Gender";
    public static final String HOBBY = "Hobby";
}
