package com.example.myapplication3;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.example.myapplication3.database.TblUserData;
import com.example.myapplication3.util.Constant;

import java.util.ArrayList;
import java.util.HashMap;

public class MainActivity extends AppCompatActivity {

    boolean doubleBackToExitPressedOnce = false;


    EditText etFirstName, etemail, etmobilenumber;
    Button etsubmit;
    TextView etdiplay;
    RadioGroup rbGender;
    RadioButton rbMale, rbFemale;
    CheckBox cbCricket, cbFootball, cbHockey;
    ImageView ivclearName, ivclearMobileNo, ivclearEmail;

    ArrayList<HashMap<String, Object>> userlist = new ArrayList<>();


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        initViewRefernce();
        clearScreen();


        etsubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (isValid()) {

                    HashMap<String, Object> map = new HashMap<>();
                    map.put(Constant.FIRST_NAME, etFirstName.getText().toString());
                    map.put(Constant.MOBILE_NUMBER, etmobilenumber.getText().toString());
                    map.put(Constant.EMAIL_ADDRESS, etemail.getText().toString());
                    map.put(Constant.GENDER, rbMale.isChecked() ? rbMale.getText().toString() : rbFemale.getText().toString());

                    String name = etFirstName.getText().toString();
                    String mobile_number = etmobilenumber.getText().toString();
                    String email = etemail.getText().toString();
                    String gender = rbMale.isChecked() ? rbMale.getText().toString() : rbFemale.getText().toString();

                    String Hobby = "";

                    if (cbCricket.isChecked()) {
                        Hobby += "," + cbCricket.getText().toString();

                    }
                    if (cbFootball.isChecked()) {
                        Hobby += "," + cbFootball.getText().toString();
                    }
                    if (cbHockey.isChecked()) {
                        Hobby += "," + cbHockey.getText().toString();
                    }

                    Hobby.replaceFirst(String.valueOf(Hobby.charAt(0)), "");

                    TblUserData tblUserData = new TblUserData(MainActivity.this);
                    long insertedId = tblUserData.insertUserData(name, mobile_number, email, gender, Hobby);

                    Toast.makeText(MainActivity.this, insertedId > 0 ? "Data Inserted Successfully" : "Somthing went wrong", Toast.LENGTH_SHORT).show();

                    Log.d("dadadfffdff", "onClick: " + insertedId);

                    map.put(Constant.HOBBY, Hobby);
                    userlist.add(map);

                    Intent intent = new Intent(MainActivity.this, Second_Activity.class);
                    intent.putExtra("UserList", userlist);
                    startActivity(intent);

                    resetData();
                }

            }
        });

        if (rbFemale.isChecked()) {
            cbCricket.setVisibility(View.VISIBLE);
            cbFootball.setVisibility(View.GONE);
            cbHockey.setVisibility(View.VISIBLE);
        }

        rbGender.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {

            @Override
            public void onCheckedChanged(RadioGroup radioGroup, int i) {
                if (i == R.id.rbActMale) {

                    cbCricket.setVisibility(View.VISIBLE);
                    cbFootball.setVisibility(View.VISIBLE);
                    cbHockey.setVisibility(View.VISIBLE);
                } else {

                    cbCricket.setVisibility(View.VISIBLE);
                    cbFootball.setVisibility(View.GONE);
                    cbHockey.setVisibility(View.VISIBLE);
                }
            }
        });


    }

    @Override
    public void onBackPressed() {
        if (doubleBackToExitPressedOnce) {
            super.onBackPressed();
            return;
        }

        this.doubleBackToExitPressedOnce = true;
        Toast.makeText(this, "Please click BACK again to exit", Toast.LENGTH_SHORT).show();

        new Handler().postDelayed(new Runnable() {

            @Override
            public void run() {
                doubleBackToExitPressedOnce = false;
            }
        }, 2000);
    }


    void initViewRefernce() {
        etFirstName = findViewById(R.id.etActFirstname);
        etemail = findViewById(R.id.etActEmailAddress);
        etsubmit = findViewById(R.id.btnActSubmit);
        etdiplay = findViewById(R.id.tvActDisplay);
        etmobilenumber = findViewById(R.id.etActMobileNumber);

        rbGender = findViewById(R.id.rgActGender);
        rbMale = findViewById(R.id.rbActMale);
        rbFemale = findViewById(R.id.rbActFemale);

        cbCricket = findViewById(R.id.cbActCricket);
        cbFootball = findViewById(R.id.cbActFootball);
        cbHockey = findViewById(R.id.cbActHockey);

        ivclearName = findViewById(R.id.ivActclearName);
        ivclearMobileNo = findViewById(R.id.ivActclearMobileNumber);
        ivclearEmail = findViewById(R.id.ivActclearEmail);
    }

    void resetData() {
        etFirstName.setText("");
        etemail.setText("");
        etmobilenumber.setText("");
        if (rbFemale.isChecked()) {
            rbFemale.setChecked(false);
        } else {
            rbMale.setChecked(false);
        }
        cbCricket.setChecked(false);
        cbHockey.setChecked(false);
        cbFootball.setChecked(false);
    }

    boolean isValid() {
        boolean flag = true;

        if (TextUtils.isEmpty(etFirstName.getText().toString().trim())) {
            etFirstName.setError(getString(R.string.error_enter_value));
            flag = false;
            etFirstName.requestFocus();
        } else {
            String fname = etFirstName.getText().toString().trim();
            String fnamePattern = "^[a-zA-Z]+";
            if (!fname.matches(fnamePattern)) {
                etFirstName.setError("Please Enter Valid Name");
                etFirstName.requestFocus();
            }
        }

        if (TextUtils.isEmpty(etmobilenumber.getText().toString().trim())) {
            etmobilenumber.setError(getString(R.string.error_enter_value));
            flag = false;
            etmobilenumber.requestFocus();
        } else {
            if (etmobilenumber.getText().toString().length() < 10) {
                etmobilenumber.setError("Enter Valid Mobile Number");
                etmobilenumber.requestFocus();
                flag = false;
            }
        }

        if (TextUtils.isEmpty(etemail.getText().toString().trim())) {
            etemail.setError(getString(R.string.error_enter_value));
            flag = false;
            etemail.requestFocus();
        } else {
            String email = etemail.getText().toString().trim();
            String emailPattern = "^[_A-Za-z0-9-]+(\\.[_A-Za-z0-9-]+)*@[A-Za-z0-9]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";
            if (!email.matches(emailPattern)) {
                etemail.setError("Enter Valid Email Address");
                flag = false;
                etemail.requestFocus();
            }
        }

        if (!(rbMale.isChecked() || rbFemale.isChecked())) {
            Toast.makeText(MainActivity.this, "Please Select Gender", Toast.LENGTH_LONG).show();
            flag = false;
        }

        if (rbMale.isChecked()) {
            if (!(cbCricket.isChecked() || cbFootball.isChecked() || cbHockey.isChecked())) {
                Toast.makeText(MainActivity.this, "Please Selected Any One Hobby", Toast.LENGTH_SHORT).show();
                flag = false;
            }
        } else {
            if (!(cbCricket.isChecked() || cbHockey.isChecked())) {
                Toast.makeText(MainActivity.this, "Please Selected Any One Hobby", Toast.LENGTH_SHORT).show();
                flag = false;
            }
        }

        return flag;
    }

    void clearScreen() {
        etFirstName.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                if (charSequence.length() > 0) {
                    ivclearName.setVisibility(View.VISIBLE);
                    ivclearName.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            etFirstName.setText("");
                        }
                    });
                } else {
                    ivclearName.setVisibility(View.GONE);
                }
            }

            @Override
            public void afterTextChanged(Editable editable) {
            }
        });

        etmobilenumber.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                if (charSequence.length() > 0) {
                    ivclearMobileNo.setVisibility(View.VISIBLE);
                    ivclearMobileNo.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            etmobilenumber.setText("");
                        }
                    });
                } else {
                    ivclearMobileNo.setVisibility(View.GONE);
                }
            }

            @Override
            public void afterTextChanged(Editable editable) {
            }
        });

        etemail.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                if (charSequence.length() > 0) {
                    ivclearEmail.setVisibility(View.VISIBLE);
                    ivclearEmail.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            etemail.setText("");
                        }
                    });
                } else {
                    ivclearEmail.setVisibility(View.GONE);
                }
            }

            @Override
            public void afterTextChanged(Editable editable) {
            }
        });
    }

}


