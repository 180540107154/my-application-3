package com.example.myapplication3;

import android.os.Bundle;
import android.widget.ListView;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.example.myapplication3.adapter.UserListAdapeter;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;

public class Second_Activity extends AppCompatActivity {

    ListView lvDisplay;
    ArrayList<HashMap<String, Object>> takenUserList = new ArrayList<>();
    UserListAdapeter userListAdapeter;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_second);

        initViewRefernce();
        bindData();
    }

    void bindData() {
        takenUserList.addAll((Collection<? extends HashMap<String, Object>>) getIntent().getSerializableExtra("UserList"));
        userListAdapeter = new UserListAdapeter(this, takenUserList);
        lvDisplay.setAdapter(userListAdapeter);
    }

    void initViewRefernce() {
        lvDisplay = findViewById(R.id.lvActDisplay);
    }
}
