package com.example.myapplication3.database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

public class TblUserData extends MyDatabase {

    public static final String TBL_USER_DATA = "Tbl_UserData";
    public static final String USER_ID = "UserId";
    public static final String NAME = "Name";
    public static final String MOBILE_NUMBER = "MobileNumber";
    public static final String EMAIL = "Email";
    public static final String GENDER = "Gender";
    public static final String HOBBY = "Hobby";

    public TblUserData(Context context) {
        super(context);
    }

    public long insertUserData(String name, String mobile_number, String email, String gender, String hobby) {

        SQLiteDatabase db = getWritableDatabase();

        long insertedDataId = 0;

        if (isPhoneNumberAvailable(mobile_number)) {
            insertedDataId = 0;
        } else {

            ContentValues cv = new ContentValues();
            cv.put(NAME, name);
            cv.put(MOBILE_NUMBER, mobile_number);
            cv.put(EMAIL, email);
            cv.put(GENDER, gender);
            cv.put(HOBBY, hobby);
            insertedDataId = db.insert(TBL_USER_DATA, null, cv);
            db.close();
            Log.d("ddfaafdf", "insertUserData: " + insertedDataId);
        }

        return insertedDataId;
    }

    public boolean isPhoneNumberAvailable(String phonenumber) {

        SQLiteDatabase db = getReadableDatabase();
        String query = "SELECT * FROM " + TBL_USER_DATA + " WHERE " + MOBILE_NUMBER + " = ? ";
        Cursor cursor = db.rawQuery(query, new String[]{phonenumber});
        cursor.moveToFirst();
        boolean isAvailable = cursor.getCount() > 0;
        cursor.close();
        return isAvailable;

    }

}
